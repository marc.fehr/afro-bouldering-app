import '../models/block.dart';

// ApiInterface is a minimal set of functions that an API implementation
// needs to define. This is really just to provide a hook for testing, but
// later we might want to wire the app up to different data sources.
abstract class ApiInterface {
  Future<Block> createBlock(Block b);
  Future<Block> getBlock(int id);
  Future<List<Block>> listBlocks(int sectorId);
  Future<void> updateBlock(Block b);
  Future<void> deleteBlock(int id);
}
