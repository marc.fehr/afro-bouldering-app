import '../models/block.dart';
import 'interface.dart';
import 'exceptions.dart';

// TestApi provides a mock test implementation of the API interface.
class TestApi implements ApiInterface {
  int lastID;
  Map<int, Block> blocks;

  TestApi() {
    this.lastID = 0;
    blocks = new Map();
  }

  Future<Block> createBlock(Block b) async {
    var nb = b.copyWith(serverID: this.lastID);

    this.lastID += 1;
    this.blocks[nb.serverID] = nb;

    return nb;
  }

  Future<Block> getBlock(int serverID) async {
    if (!this.blocks.containsKey(serverID)) {
      throw ApiException(500, "block id not found");
    }

    return this.blocks[serverID];
  }

  Future<List<Block>> listBlocks(int sectorID) async {
    var bs = <Block>[];
    this.blocks.forEach((id, b) {
      bs.add(b);
    });

    return bs;
  }

  Future<void> updateBlock(Block b) {
    if (!this.blocks.containsKey(b.serverID)) {
      throw ApiException(500, "block id not found");
    }

    this.blocks[b.serverID] = b;
  }

  Future<void> deleteBlock(int serverID) {
    if (!this.blocks.containsKey(serverID)) {
      throw ApiException(500, "block id not found");
    }

    this.blocks.remove(serverID);
  }
}
