import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/block.dart';
import 'interface.dart';
import 'exceptions.dart';

// RestApi provides an implementation of the API interface that hits
// Maciej's server.
class RestApi implements ApiInterface {
  static final _url = 'https://afro.baroquesoftware.com';

  static Future<String> _get(String path) async {
    var res = await http.get(_url + path);
    if (res.statusCode != 200) {
      throw ApiException.fromResponse(res);
    }

    return res.body;
  }

  static Future<String> _post(String path, Map<String, dynamic> body) async {
    var res = await http.post(_url + path, body: body);
    if (res.statusCode != 200) {
      throw ApiException.fromResponse(res);
    }

    return res.body;
  }

  Future<Block> createBlock(Block b) async {
    var res = await _post('/block/add', {
      'sector': '${b.sector}',
      'lat': '${b.lat}',
      'lon': '${b.lon}',
      'name': b.name,
      'description': b.description,
    });

    var id = json.decode(res)['id'];
    return this.getBlock(id);
  }

  Future<Block> getBlock(int serverID) async {
    var res = await _get('/block/${serverID}');

    return Block.fromJson(res).copyWith(serverID: serverID);
  }

  Future<List<Block>> listBlocks(int sectorID) async {
    var res = await _get('/block/list?q=sector:${sectorID}');
    var data = json.decode(res)['blocks'];

    var blocks = <Block>[];
    data.forEach((blockData) => blocks.add(Block.fromMap(blockData)));
    return blocks;
  }

  Future<void> updateBlock(Block b) async {
    await _post('/block/${b.serverID}', {
      'lat': '${b.lat}',
      'lon': '${b.lon}',
      'name': b.name,
      'description': b.description,
    });
  }

  Future<void> deleteBlock(int serverID) async {
    await _post('/block/delete', {'id': '$serverID'});
  }
}
