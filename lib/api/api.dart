import 'dart:convert';

import 'package:http/http.dart' as http;

import '../models/block.dart';
import 'exceptions.dart';
import 'interface.dart';
import 'rest_api.dart';

// Api exposes a singleton implementation of ApiInterface. By default
// it's set to the REST implementation, but can be changed for testing
// or to hit a different data endpoint if desired.
abstract class Api {
  static ApiInterface _impl = new RestApi();

  static void swapImplementation(ApiInterface impl) {
    Api._impl = impl;
  }

  static Future<Block> createBlock(Block b) {
    return Api._impl.createBlock(b);
  }

  static Future<Block> getBlock(int serverID) {
    return Api._impl.getBlock(serverID);
  }

  static Future<List<Block>> listBlocks(int sectorID) {
    return Api._impl.listBlocks(sectorID);
  }

  static Future<void> updateBlock(Block b) {
    return Api._impl.updateBlock(b);
  }

  static Future<void> deleteBlock(int serverID) {
    return Api._impl.deleteBlock(serverID);
  }
}
