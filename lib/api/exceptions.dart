import 'dart:convert';

import 'package:http/http.dart' as http;

// ApiException represents a non-200 response from an API. This assumes that
// the implementation is a HTTP server, but we can generalise if we need to.
class ApiException implements Exception {
  int code;
  String msg;

  ApiException(int _code, String _msg) {
    code = _code;
    msg = _msg;
  }

  factory ApiException.fromResponse(http.Response res) {
    var msg = json.decode(res.body)["status"];

    return new ApiException(res.statusCode, msg);
  }

  String error() => this.msg;
}
