import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:hive/hive.dart' as hive;
import 'package:hive_flutter/hive_flutter.dart' as hive_flutter;
import 'package:uuid/uuid.dart';

import '../models/block.dart';

// HiveNotifier manages the blocks/problems/images stored in a local
// hive database. Each box provides a particular data model, and an
// incrementing counter for each open box lets ChangeListeners detect
// which boxes were modified after the most recent notification.
// TODO: probably integrate with block provider once sync is working.
//       having staging versions of everything here would be hell.
class HiveNotifier with ChangeNotifier {
  // _blockBoxUri should be updated if there are any breaking changes
  // to the Block model, as it keeps old blocks in local storage.
  static const String _blockBoxUri = 'blocks_v1.2';

  // UUID generator for local model IDs.
  Uuid _uuid;

  // State for the block models.
  int blockChanges;
  hive.Box<Block> _blockBox;
  Map<int, String> _blockIDLedger; // server IDs to local UUIDs

  // _create is private on purpose - rather use newHiveNotifier().
  HiveNotifier._create(hive.Box<Block> _blockBox) {
    this._uuid = Uuid();

    this.blockChanges = 0;
    this._blockBox = _blockBox;
    this._blockIDLedger = new Map<int, String>();

    // Make sure we have an up-to-date ledger of IDs.
    for (var b in this.getBlocks()) {
      this._blockIDLedger[b.serverID] = b.id;
    }
  }

  // init initialises the connection to the local hive store and
  // registers all of our type adapters.
  static Future<void> init([String dir]) async {
    hive.Hive.registerAdapter(BlockAdapter());

    if (dir == null) {
      await hive.Hive.initFlutter();
    } else {
      await hive.Hive.init(dir);
    }
  }

  // newHiveNotifier initialises and creates a new HiveNotifier instance.
  static Future<HiveNotifier> newHiveNotifier() async {
    var blockBox = await hive.Hive.openBox<Block>(_blockBoxUri);
    return HiveNotifier._create(blockBox);
  }

  // putBlock adds the given block to the store, generating a local ID for it
  // if it doesn't already have one.
  Future<Block> putBlock(Block b) async {
    var nb = await this._putBlock(b);

    // Update the nonce and notify listeners that something changed.
    this.blockChanges += 1;
    notifyListeners();

    return nb;
  }

  // putBlocks adds all of the given blocks to the store, generating local
  // IDs for any that don't already have.
  Future<List<Block>> putBlocks(List<Block> bs) async {
    var res = List<Block>();
    for (var b in bs) {
      res.add(await this._putBlock(b));
    }

    // Update the nonce and notify listeners that something changed.
    this.blockChanges += 1;
    notifyListeners();

    return res;
  }

  // getBlock returns the block with the given local ID.
  Block getBlock(String id) {
    return this._blockBox.get(id);
  }

  // getBlocks returns all of the blocks in the store.
  List<Block> getBlocks() {
    List<Block> res = [];
    for (var id in this._blockBox.keys) {
      res.add(this._blockBox.get(id));
    }

    return res;
  }

  // deleteBlock removes the given block from the store.
  Future<void> deleteBlock(String id) async {
    var b = getBlock(id);
    if (b == null) {
      return;
    }

    await this._blockBox.delete(id);
    this._blockIDLedger.remove(b.serverID);

    // Update the nonce and notify listeners that something changed.
    this.blockChanges += 1;
    notifyListeners();
  }

  // _putBlock puts the given block in the store without notifying anyone.
  Future<Block> _putBlock(Block b) async {
    // If a block has a serverID, but our cache has the local id wrong,
    // something has gone fatally wrong in the data logic.
    if (b.id != null &&
        this._blockIDLedger[b.serverID] != null &&
        b.id != this._blockIDLedger[b.serverID]) {
      throw Exception(
          'Fatal error in hive store - block has inconsistent local ID');
    }

    // We want to reuse the same local IDs as much as possible so we
    // don't duplicate blocks on server updates and such.
    var id = b.id ?? this._blockIDLedger[b.serverID] ?? this._uuid.v4();
    var nb = b.copyWith(id: id);
    await this._blockBox.put(nb.id, nb);

    // If we have a serverID, we should record the block in the ledger.
    if (b.serverID != null) {
      this._blockIDLedger[b.serverID] = id;
    }

    return nb;
  }
}
