// import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import './hive.dart';
import '../api/api.dart';
import '../models/block.dart';

class BlockProvider with ChangeNotifier {
  HiveNotifier _hn;
  int _blockChanges;

  BlockProvider(HiveNotifier hn) {
    this._hn = hn;
    this._blockChanges = hn.blockChanges;

    // Listen for updates to the block store.
    this._hn.addListener(() {
      var newBlockChanges = this._hn.blockChanges;
      if (newBlockChanges > this._blockChanges) {
        this._blockChanges = newBlockChanges;
        notifyListeners();
      }
    });
  }

  Future<void> addBlock(Block b) {
    return this._hn.putBlock(nb);
  }

  Block getBlock(String id) {
    return this._hn.getBlock(id);
  }

  List<Block> getBlocks() {
    return this._hn.getBlocks();
  }

  Future<void> refreshBlocks() async {
    await this._hn.putBlocks(await Api.listBlocks(0));
  }

  Future<void> updateBlock(Block b) {
    return this._hn.putBlock(b.copyWith(isDirty: true));
  }

  // TODO: offline syncing support
  Future<void> deleteBlock(String id) async {
    var b = getBlock(id);
    if (b == null) {
      return;
    }

    await Api.deleteBlock(b.serverID);
    await this._hn.deleteBlock(id);
  }
}
