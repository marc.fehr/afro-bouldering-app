import 'package:AFRO/screens/contribute_problem_draw_line.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'providers/blocks.dart';
import 'providers/hive.dart';
import 'sync/block.dart';
import 'screens/contribute_main_screen.dart';
import 'screens/main_screen.dart';
import 'screens/settings_screen.dart';
import 'screens/contribute_block_edit_screen.dart';
import 'screens/contribute_block_image_picker.dart';
import 'screens/contribute_block_images_picker.dart';
import 'screens/contribute_problem_draw_line.dart';

void main() async {
  // Initialise local storage DB.
  await HiveNotifier.init();
  await App.init();

  // Lock app to only run in portrait mode (upside down or normal).
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ],
  );

  // Let's go!
  runApp(App());
}

class App extends StatelessWidget {
  // Local storage DB.
  static HiveNotifier _hn;

  // Local storage to server sync routines.
  static BlockSyncer _bs;

  static Future<void> init() async {
    App._hn = await HiveNotifier.newHiveNotifier();
    App._bs = new BlockSyncer(App._hn);
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => BlockProvider(App._hn),
        ),
      ],
      child: MaterialApp(
        title: 'AFRO',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          accentColor: Colors.deepOrange,
          fontFamily: 'Lato',
        ),
        home: MainScreen(),
        routes: {
          ContributeScreen.routeName: (ctx) => ContributeScreen(),
          SettingsScreen.routeName: (ctx) => SettingsScreen(),
          EditBlockScreen.routeName: (ctx) => EditBlockScreen(),
          ImagePickerScreen.routeName: (ctx) => ImagePickerScreen(),
          ImagesPickerScreen.routeName: (ctx) => ImagesPickerScreen(),
          EditProblemDrawLineScreen.routeName: (ctx) =>
              EditProblemDrawLineScreen(),
        },
      ),
    );
  }
}
