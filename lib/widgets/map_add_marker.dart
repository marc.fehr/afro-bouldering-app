import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map_location/flutter_map_location.dart';

class AddMarkerToMap extends StatefulWidget {
  static const String route = '/add-marker';
  final Function callback;
  final double lat;
  final double lon;

  AddMarkerToMap({
    this.callback,
    this.lat,
    this.lon,
  });

  @override
  State<StatefulWidget> createState() {
    return AddMarkerToMapState();
  }
}

class AddMarkerToMapState extends State<AddMarkerToMap> {
  List<LatLng> tappedPoints = [];
  final MapController mapController = MapController();
  final List<Marker> userLocationMarkers = <Marker>[];
  var _currentZoom = 15.0;
  var _currentCenter = LatLng(-34.106430, 18.468122); // MUIZENBERG COORDS

  // Using map controller to zoom in when clicking on cluster...
  void _zoom(String direction, double amount) {
    if (direction == "out") {
      setState(() {
        _currentZoom = _currentZoom - amount;
      });
    } else if (direction == "in") {
      setState(() {
        _currentZoom = _currentZoom + amount;
      });
    } else if (direction == "set") {
      setState(() {
        _currentZoom = amount;
      });
    }

    // Reset map zoom by using move method on _mapController
    mapController.onReady.then((result) {
      mapController.move(_currentCenter, _currentZoom);
    });
  }

  @override
  void initState() {
    if (widget.lat != null && widget.lon != null) {
      tappedPoints = [LatLng(widget.lat, widget.lon)];
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var markers = tappedPoints.map(
      (latlng) {
        return Marker(
          width: 40.0,
          height: 40.0,
          point: latlng,
          builder: (ctx) => Container(
            child: Icon(
              Icons.location_pin,
              color: Theme.of(context).primaryColor,
              size: 40,
            ),
          ),
        );
      },
    ).toList();

    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        children: [
          Flexible(
            child: FlutterMap(
              mapController: mapController,
              options: MapOptions(
                center: _currentCenter,
                zoom: _currentZoom,
                onTap: _handleTap,
                plugins: <MapPlugin>[
                  LocationPlugin(),
                ],
              ),
              layers: [
                TileLayerOptions(
                  urlTemplate:
                      'https://api.mapbox.com/styles/v1/mrcfhr/ckknmknsa5uvx17nw72e4mxxf/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNqeWx2eWx3MDBicm0zY3J0YnRla25yN2UifQ.CKfTqZTXfSZie6mJHjU0OQ',
                  additionalOptions: {
                    'accessToken':
                        'pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNra2wzOTV2NzE2Z3oydnRkN25jdHU4bmUifQ.CBGjh0EER92aQciztUuryg',
                    'id': 'mapbox.satellite',
                  },
                ),
                MarkerLayerOptions(
                  markers: [...markers, ...userLocationMarkers],
                ),
                LocationOptions(
                  markers: userLocationMarkers,
                  onLocationUpdate: (LatLngData ld) {
                    print('Location updated: ${ld?.location}');
                  },
                  onLocationRequested: (LatLngData ld) {
                    if (ld == null || ld.location == null) {
                      return;
                    }
                    _zoom("set", 12.0);
                    mapController?.move(ld.location, _currentZoom);
                  },
                  markerBuilder: (BuildContext context, LatLngData ld,
                      ValueNotifier<double> heading) {
                    return Marker(
                      point: ld.location,
                      builder: (_) => Container(
                        child: Column(
                          children: <Widget>[
                            Stack(
                              alignment: AlignmentDirectional.center,
                              children: <Widget>[
                                Container(
                                  // decoration: BoxDecoration(
                                  //     shape: BoxShape.circle,
                                  //     color: Colors.pink[300].withOpacity(0.7)),
                                  height: 50.0,
                                  width: 50.0,
                                ),
                                const Icon(
                                  Icons.gps_fixed,
                                  color: Colors.white,
                                  size: 25.0,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      height: 60.0,
                      width: 60.0,
                    );
                  },
                  buttonBuilder: (BuildContext context,
                      ValueNotifier<LocationServiceStatus> status,
                      Function onPressed) {
                    return Align(
                      alignment: Alignment.bottomRight,
                      child: Padding(
                        padding:
                            const EdgeInsets.only(bottom: 16.0, right: 16.0),
                        child: FloatingActionButton(
                          child: ValueListenableBuilder<LocationServiceStatus>(
                              valueListenable: status,
                              builder: (BuildContext context,
                                  LocationServiceStatus value, Widget child) {
                                switch (value) {
                                  case LocationServiceStatus.disabled:
                                  case LocationServiceStatus.permissionDenied:
                                  case LocationServiceStatus.unsubscribed:
                                    return const Icon(
                                      Icons.location_disabled,
                                      color: Colors.white,
                                    );
                                    break;
                                  default:
                                    return const Icon(
                                      Icons.location_searching,
                                      color: Colors.white,
                                    );
                                    break;
                                }
                              }),
                          onPressed: () => onPressed(),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(tappedPoints.length <= 0
                    ? "LatLng not set"
                    : "LatLng: ${tappedPoints[0].latitude.toStringAsFixed(6)} / ${tappedPoints[0].latitude.toStringAsFixed(6)}"),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _handleTap(LatLng latlng) {
    setState(() {
      // tappedPoints.add(latlng);
      tappedPoints = [latlng];
      widget.callback(latlng);
      // Send position to parent using callback, TODO(Marc): Set to global state?
    });
  }
}
