import "package:flutter/material.dart";
// import "package:provider/provider.dart";

// import "../providers/problems.dart";

class ProblemItem extends StatelessWidget {
  final int id;
  final String name;
  final bool isEditable;

  ProblemItem({
    @required this.id,
    @required this.name,
    this.isEditable,
  });

  @override
  Widget build(BuildContext context) {
    // final problemsProvider = Provider.of<Problems>(context);

    return ListTile(
      title: Text(name),
      subtitle: Text("Static subtitle of problem"),
      trailing: isEditable
          ? Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        "/edit-problem",
                        arguments: id,
                      );
                    },
                    color: Theme.of(context).primaryColor,
                  ),
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      // problemsProvider.deleteProblem(id);
                    },
                    color: Theme.of(context).errorColor,
                  ),
                ],
              ),
            )
          : Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("500m"),
                  SizedBox(
                    width: 10,
                    // Gives some space
                  ),
                  CircleAvatar(
                    child: Text(
                      "ID",
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
