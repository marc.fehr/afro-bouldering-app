import "package:flutter/material.dart";
import "package:provider/provider.dart";

import "../providers/blocks.dart";
import "../widgets/block_item.dart";

class ListBlocks extends StatelessWidget {
  const ListBlocks({Key key}) : super(key: key);

  Future<void> _refreshBlocks(BuildContext ctx) async {
    await Provider.of<BlockProvider>(
      ctx,
      listen: false,
    ).fetchBlocksBySectorAndStoreInProvider();
  }

  @override
  Widget build(BuildContext context) {
    final blocks = Provider.of<BlockProvider>(context).getBlocks();

    return RefreshIndicator(
      onRefresh: () => _refreshBlocks(context),
      child: Container(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListView.builder(
            itemBuilder: (_, i) => Column(
              children: [
                BlockItem(
                  id: blocks[i].id,
                  name: blocks[i].name,
                  isEditable: false,
                ),
                Divider(),
              ],
            ),
            itemCount: blocks.length,
          ),
        ),
      ),
    );
  }
}
