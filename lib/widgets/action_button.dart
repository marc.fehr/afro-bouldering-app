import "package:flutter/material.dart";

class ActionButton extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget icon;
  final Function action;

  ActionButton({
    @required this.title,
    @required this.subtitle,
    @required this.icon,
    @required this.action,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      // child: Card(
      // clipBehavior: Clip.hardEdge,
      // elevation: 1,
      child: ListTile(
        onTap: action,
        title: Text(title),
        subtitle: Text(subtitle),
        leading: Container(
          height: double.infinity,
          child: icon,
        ),
        // enableFeedback: true,
        // trailing: Text("XXX"),
      ),
    );
    // );
  }
}
