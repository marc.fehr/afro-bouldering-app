import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter_map_marker_cluster/flutter_map_marker_cluster.dart';
import 'package:flutter_map_location/flutter_map_location.dart';

import '../providers/blocks.dart';
import '../models/block.dart';

class MapWidget extends StatefulWidget {
  @override
  _MapWidgetState createState() => _MapWidgetState();
}

class _MapWidgetState extends State<MapWidget> {
  var _currentZoom = 8.0;
  var _currentCenter = LatLng(-34.106430, 18.468122);

  // Creates map controller so we can control map programmatically
  MapController _mapController = MapController();

  // Creates list for user markers
  final List<Marker> userLocationMarkers = <Marker>[];

  // Using map controller to zoom in when clicking on cluster...
  void _zoom(String direction, double amount) {
    if (direction == "out") {
      setState(() {
        _currentZoom = _currentZoom - amount;
      });
    } else if (direction == "in") {
      setState(() {
        _currentZoom = _currentZoom + amount;
      });
    } else if (direction == "set") {
      setState(() {
        _currentZoom = amount;
      });
    }

    // Reset map zoom by using move method on _mapController
    _mapController.onReady.then((result) {
      _mapController.move(_currentCenter, _currentZoom);
    });
  }

  // Bottom sheet for displaying block information
  void displayBottomSheet(BuildContext context, Block block) {
    showModalBottomSheet(
        elevation: 3,
        isDismissible: true,
        // backgroundColor: Theme.of(context).primaryColor,
        context: context,
        builder: (ctx) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.4, // 40% height
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  '${block.name}',
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Column(
                    children: <Widget>[
                      Text('This block has ID ${block.id}'),
                      Text('${block.lat} / ${block.lon}'),
                    ],
                  ),
                ),
                SizedBox(height: 10),
                ElevatedButton.icon(
                  icon: Icon(Icons.image),
                  label: Text('Show topos'),
                  onPressed: () {
                    print('Here we need to link to the block detail screen...');
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    var blocks = Provider.of<BlockProvider>(context, listen: false).getBlocks();

    var _blockMarkers = blocks
        .map(
          (block) => Marker(
            width: 45.0,
            height: 45.0,
            point: LatLng(block.lat, block.lon),
            builder: (context) => Container(
              child: IconButton(
                icon: Icon(
                  Icons.location_pin,
                  color: Theme.of(context).primaryColor,
                  size: 45.0,
                ),
                onPressed: () {
                  displayBottomSheet(context, block);
                },
              ),
            ),
          ),
        )
        .toList();

    return Container(
      child: FlutterMap(
        mapController: _mapController,
        options: MapOptions(
          minZoom: 4.0,
          zoom: 8.0,
          center: _currentCenter,
          plugins: <MapPlugin>[
            MarkerClusterPlugin(), // Adds plugin for marker clustering
            LocationPlugin(),
          ],
        ),
        layers: [
          TileLayerOptions(
            // urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
            // subdomains: [
            //   'a',
            //   'b',
            //   'c',
            // ],
            urlTemplate:
                'https://api.mapbox.com/styles/v1/mrcfhr/ckknmknsa5uvx17nw72e4mxxf/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNqeWx2eWx3MDBicm0zY3J0YnRla25yN2UifQ.CKfTqZTXfSZie6mJHjU0OQ',
            additionalOptions: {
              'accessToken':
                  'pk.eyJ1IjoibXJjZmhyIiwiYSI6ImNra2wzOTV2NzE2Z3oydnRkN25jdHU4bmUifQ.CBGjh0EER92aQciztUuryg',
              'id': 'mapbox.satellite',
            },
          ),
          // This is where the clustering happens on the map for the blocks...
          MarkerClusterLayerOptions(
            maxClusterRadius: 120,
            disableClusteringAtZoom: 8,
            size: Size(40, 40),
            anchor: AnchorPos.align(AnchorAlign.center),
            fitBoundsOptions: FitBoundsOptions(
              padding: EdgeInsets.all(50),
            ),
            markers: _blockMarkers,
            builder: (context, _clusterMarkers) {
              return FloatingActionButton.extended(
                heroTag: "location-button",
                elevation: 2,
                splashColor: Theme.of(context).accentColor,
                onPressed: () => _zoom("in", 1.0),
                label: Text(
                  _clusterMarkers.length.toString(),
                  style: TextStyle(
                    color: Theme.of(context).accentColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                icon: null,
                backgroundColor: Theme.of(context).primaryColor,
              );
            },
          ),
          // Implements the user location plugin
          LocationOptions(
            markers: userLocationMarkers,
            onLocationUpdate: (LatLngData ld) {
              print('Location updated: ${ld?.location}');
            },
            onLocationRequested: (LatLngData ld) {
              if (ld == null || ld.location == null) {
                return;
              }
              _zoom("set", 12.0);
              _mapController?.move(ld.location, _currentZoom);
            },
            buttonBuilder: (BuildContext context,
                ValueNotifier<LocationServiceStatus> status,
                Function onPressed) {
              return Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, right: 16.0),
                  child: FloatingActionButton(
                      child: ValueListenableBuilder<LocationServiceStatus>(
                          valueListenable: status,
                          builder: (BuildContext context,
                              LocationServiceStatus value, Widget child) {
                            switch (value) {
                              case LocationServiceStatus.disabled:
                              case LocationServiceStatus.permissionDenied:
                              case LocationServiceStatus.unsubscribed:
                                return const Icon(
                                  Icons.location_disabled,
                                  color: Colors.white,
                                );
                                break;
                              default:
                                return const Icon(
                                  Icons.location_searching,
                                  color: Colors.white,
                                );
                                break;
                            }
                          }),
                      onPressed: () => onPressed()),
                ),
              );
            },
          ),
          // Adds markers for user location to map
          MarkerLayerOptions(markers: userLocationMarkers),
        ],
      ),
    );
  }
}
