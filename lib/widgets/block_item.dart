import "package:flutter/material.dart";
import "package:provider/provider.dart";

import "../providers/blocks.dart";

class BlockItem extends StatelessWidget {
  final String id;
  final String name;
  final bool isEditable;
  // final String imageUrl;

  BlockItem({
    @required this.id,
    @required this.name,
    this.isEditable,
  });

  @override
  Widget build(BuildContext context) {
    final blocksProvider = Provider.of<BlockProvider>(context);

    return ListTile(
      title: Text(name), // TODO(Marc): Create UI for dismissible behaviour
      subtitle: Text(blocksProvider.getBlock(id).description),
      trailing: isEditable
          ? Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.edit),
                    // TODO: Implement update methods on API first...
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        "/edit-block",
                        arguments: id,
                      );
                    },
                    color: Theme.of(context).primaryColor,
                  ),
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      blocksProvider.deleteBlock(id);
                    },
                    color: Theme.of(context).errorColor,
                  ),
                ],
              ),
            )
          : Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text("500m"),
                  SizedBox(
                    width: 10,
                    // Gives some space
                  ),
                  CircleAvatar(
                    child: Text(
                      blocksProvider.getBlock(id).id.toString(),
                      // Adds block ID in circular coloured are to the right
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
