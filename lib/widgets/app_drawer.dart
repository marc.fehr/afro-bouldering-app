import "package:flutter/material.dart";

// Import screens to get routeName attributes
import '../screens/contribute_main_screen.dart';
import "../screens/settings_screen.dart";

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text(
              "What's your plan?",
            ),
            automaticallyImplyLeading: false,
          ),
          SizedBox(height: 10),
          ListTile(
            leading: Icon(Icons.architecture),
            title: Text("Find existing climbs"),
            onTap: () {
              // Go to screen set as home in main.dart
              Navigator.of(context).pushReplacementNamed("/");
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.add_location_alt_rounded),
            title: Text("Manage climbs in database"),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(ContributeScreen.routeName);
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text("Edit user settings"),
            onTap: () {
              Navigator.of(context)
                  .pushReplacementNamed(SettingsScreen.routeName);
            },
          ),
        ],
      ),
    );
  }
}
