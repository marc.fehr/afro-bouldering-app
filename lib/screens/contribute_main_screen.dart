import 'package:flutter/material.dart';

import './contribute_block_overview_screen.dart';
import './contribute_block_edit_screen.dart';
import './contribute_problem_overview_screen.dart';
import './contribute_problem_draw_line.dart';
// import './contribute_problem_edit_screen.dart';
import '../widgets/app_drawer.dart';

class ContributeScreen extends StatefulWidget {
  static const routeName = '/contribute';

  @override
  _ContributeScreenState createState() => _ContributeScreenState();
}

class _ContributeScreenState extends State<ContributeScreen> {
  List<Map<String, dynamic>> _pages;

  @override
  void initState() {
    _pages = [
      // TODO: Replace all with proper external screen widgets
      {
        'page': Text('Add, edit or delete an area'),
        'title': 'Edit area',
        'action': () {
          print('Action triggered!');
        }
      },
      {
        'page': Text(
          'Add, edit or delete a sector',
        ),
        'title': 'Edit sector',
        'action': () {
          print('Action triggered!');
        }
      },
      {
        'page': ContributeBlockScreen(),
        'title': 'Edit block',
        'action': () {
          Navigator.of(context).pushNamed(EditBlockScreen.routeName);
        }
      },
      {
        'page': ContributeProblemScreen(),
        'title': 'Edit problem',
        'action': () {
          // Navigator.of(context).pushNamed(EditProblemScreen.routeName);
          Navigator.of(context).pushNamed(EditProblemDrawLineScreen.routeName);
        }
      },
    ];
    super.initState();
  }

  int _selectedPageIndex = 2;

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title']),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: _pages[_selectedPageIndex]['action'],
          ),
        ],
      ),
      body: Center(
        child: _pages.elementAt(_selectedPageIndex)['page'],
      ),
      drawer: AppDrawer(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _pages[_selectedPageIndex]['action'],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        unselectedItemColor: Theme.of(context).primaryColor,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.explore),
            label: 'Area',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.texture),
            label: 'Sector',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.terrain),
            label: 'Block',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.timeline),
            label: 'Problem',
          ),
        ],
        currentIndex: _selectedPageIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _selectPage,
      ),
    );
  }
}
