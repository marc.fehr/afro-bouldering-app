import "package:flutter/material.dart";

import "../widgets/app_drawer.dart";

class SettingsScreen extends StatelessWidget {
  static const routeName = "/settings";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User settings"),
      ),
      body: Center(
        child: Text("Settings screen"),
      ),
      drawer: AppDrawer(),
    );
  }
}
