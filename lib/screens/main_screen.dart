import "package:provider/provider.dart";
import "package:flutter/material.dart";

import "../providers/blocks.dart";
import "../widgets/app_drawer.dart";
import "../widgets/list_blocks.dart";
import "../widgets/map_widget.dart";

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  var _isLoading = true;
  var _showMap = true;

  @override
  void initState() {
    // Don't use async here as it always returns Futures

    // Fetch data here
    Provider.of<BlockProvider>(context, listen: false)
        .fetchBlocksBySectorAndStoreInProvider()
        .then((_) {
      setState(
        () {
          _isLoading = false;
        },
      );
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("AFRO"),
        actions: <Widget>[
          IconButton(
            icon: _showMap
                ? const Icon(Icons.list_alt_sharp)
                : const Icon(Icons.map_outlined),
            onPressed: () {
              setState(
                () {
                  _showMap = !_showMap;
                },
              );
            },
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: Center(
        child: _showMap
            ? MapWidget()
            : _isLoading
                ? CircularProgressIndicator()
                : ListBlocks(),
      ),
    );
  }
}
