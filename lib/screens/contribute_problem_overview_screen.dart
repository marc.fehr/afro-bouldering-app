import "package:flutter/material.dart";
// import "package:provider/provider.dart";

// TODO: Create providers and models for problem first
// import "../providers/problems.dart";
// import "../models/problem.dart";
import '../widgets/problem_item.dart';

class ContributeProblemScreen extends StatefulWidget {
  const ContributeProblemScreen({Key key}) : super(key: key);

  @override
  _ContributeProblemScreenState createState() =>
      _ContributeProblemScreenState();
}

class _ContributeProblemScreenState extends State<ContributeProblemScreen> {
  String filter;
  // List<Problem> _filteredProblems; // TODO(Marc): Set filtered problems based on input
  TextEditingController _searchTextController;

  // Future<void> _refreshProblems(BuildContext ctx) async {
  //   await Provider.of<Problems>(
  //     ctx,
  //     listen: false,
  //   ).fetchProblemsByBlockAndStoreInProvider();
  // }

  _printFiltervalue() {
    print(_searchTextController.text);
  }

  @override
  void initState() {
    super.initState();
    _searchTextController = TextEditingController();

    _searchTextController.addListener(
      () {
        _printFiltervalue();
        setState(() {
          filter = _searchTextController.text;
        });
        // _filteredProblems =
        //     Provider.of<Problems>(context, listen: false).problems;
      },
    );
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final problems = Provider.of<Problems>(context).problems;
    // final _searchTextController = TextEditingController();

    return RefreshIndicator(
      // onRefresh: () => _refreshProblems(context),
      onRefresh: () {
        print("Refresh problems here");
        return null;
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: ListTile(
                  title: TextField(
                    controller: _searchTextController,
                    // onChanged: (val) {},
                    // TODO(Marc): Implement logic and state changes for filter
                    decoration: const InputDecoration(
                      suffixIcon: Icon(
                        Icons.search,
                        color: Colors.black,
                        size: 20.0,
                      ),
                      border: InputBorder.none,
                      hintText: 'Filter problems by name...',
                    ),
                  ),
                ),
              ),
            ),
            // Expanded(
            //   child: ListView.builder(
            //     itemBuilder: (_, i) => Column(
            //       children: [
            //         ProblemItem(
            //           id: problems[i].id,
            //           name: problems[i].name,
            //           isEditable: true,
            //         ),
            //         Divider(),
            //       ],
            //     ),
            //     itemCount: problems.length,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
