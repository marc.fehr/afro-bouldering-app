import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

class ImagePickerScreen extends StatefulWidget {
  static var routeName = '/image-picker';
  ImagePickerScreen() : super();

  final String title = "Upload Image Demo";

  @override
  ImagePickerScreenState createState() => ImagePickerScreenState();
}

class ImagePickerScreenState extends State<ImagePickerScreen> {
  File _image;
  Image _imageFile;
  final picker = ImagePicker();

  void resetImage() {
    setState(() {
      _image = null;
      _imageFile = null;
    });
  }

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    final imageFile = Image.memory(await pickedFile.readAsBytes());

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        print(_image);
        _imageFile = imageFile;
        print(_imageFile);
      } else {
        print('No image selected.');
      }
    });
  }

  Future<String> uploadImage(filename) async {
    print(filename);
    final String url = "https://afro.baroquesoftware.com/photo/add";
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(await http.MultipartFile.fromPath('picture', filename));
    var res = await request.send();
    // print(res.reasonPhrase);
    return res.reasonPhrase;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Image Picker Example'),
      ),
      body: Center(
        child: _image == null
            ? Text('No image selected.')
            : Column(
                children: <Widget>[
                  Image.file(_image),
                  ListTile(
                    leading: Icon(Icons.cloud_upload),
                    title: Text("Upload this photo to the server..."),
                    onTap: () {
                      // Upload image here
                      uploadImage(_image.path);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.delete),
                    title: Text("Reset photo"),
                    onTap: () {
                      // Reset
                      resetImage();
                    },
                  ),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
