import "package:flutter/material.dart";
import "package:provider/provider.dart";

// import "./contribute_block_edit_screen.dart";
import "../providers/blocks.dart";
import "../models/block.dart";
// import '../widgets/action_button.dart';
import '../widgets/block_item.dart';

class ContributeBlockScreen extends StatefulWidget {
  const ContributeBlockScreen({Key key}) : super(key: key);

  @override
  _ContributeBlockScreenState createState() => _ContributeBlockScreenState();
}

class _ContributeBlockScreenState extends State<ContributeBlockScreen> {
  String filter;
  List<Block> _filteredBlocks; // TODO(Marc): Set filtered blocks based on input
  TextEditingController _searchTextController;

  Future<void> _refreshBlocks(BuildContext ctx) async {
    await Provider.of<BlockProvider>(
      ctx,
      listen: false,
    ).fetchBlocksBySectorAndStoreInProvider();
  }

  _printFiltervalue() {
    print(_searchTextController.text);
  }

  @override
  void initState() {
    super.initState();
    _searchTextController = TextEditingController();

    _searchTextController.addListener(() {
      _printFiltervalue();
      setState(() {
        filter = _searchTextController.text;
      });
      _filteredBlocks =
          Provider.of<BlockProvider>(context, listen: false).getBlocks();
    });
  }

  @override
  void dispose() {
    _searchTextController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final blocks = Provider.of<BlockProvider>(context).getBlocks();
    final _searchTextController = TextEditingController();

    return RefreshIndicator(
      onRefresh: () => _refreshBlocks(context),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: ListTile(
                  title: TextField(
                    controller: _searchTextController,
                    // onChanged: (val) {},
                    // TODO(Marc): Implement logic and state changes for filter
                    decoration: const InputDecoration(
                      suffixIcon: Icon(
                        Icons.search,
                        color: Colors.black,
                        size: 20.0,
                      ),
                      border: InputBorder.none,
                      hintText: 'Filter blocks by name...',
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemBuilder: (_, i) => Column(
                  children: [
                    BlockItem(
                      id: blocks[i].id,
                      name: blocks[i].name,
                      isEditable: true,
                    ),
                    Divider(),
                  ],
                ),
                itemCount: blocks.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}
