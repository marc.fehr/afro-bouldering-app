import 'dart:async';
import 'dart:typed_data';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class ImagesPickerScreen extends StatefulWidget {
  static var routeName = '/images-picker';

  @override
  ImagesPickerScreenState createState() => ImagesPickerScreenState();
}

class ImagesPickerScreenState extends State<ImagesPickerScreen> {
  List<Asset> _images = <Asset>[];
  Error _error;

  _uploadImagesToServer() async {
    _images.forEach(
      (image) => {
        print(image.getByteData()),
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(_images.length, (index) {
        Asset asset = _images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> _loadAssets() async {
    List<Asset> resultList = <Asset>[];
    // String error = 'No Error Dectected';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: _images,
        cupertinoOptions: CupertinoOptions(takePhotoIcon: 'chat'),
        materialOptions: MaterialOptions(
          actionBarColor: '#abcdef',
          actionBarTitle: 'Example App',
          allViewTitle: 'All Photos',
          useDetailsView: false,
          selectCircleStrokeColor: '#000000',
        ),
      );
    } on Exception catch (error) {
      print(error);
      throw (error);
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(
      () {
        _images = resultList;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // Getting callback function to pass images back...
    final Function callback = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Select photos'),
      ),
      body: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.add_a_photo),
            title: Text("Pick images"),
            onTap: _loadAssets,
          ),
          ListTile(
            enabled: (_images.length > 0 && _error == null),
            leading: Icon(Icons.save),
            title: (_images.length > 0 && _error == null)
                ? Text("Save to local storage and go back")
                : Text("No images selected"),
            onTap: () {
              // TODO(Guy or Marc): Save images to localStorage
              // TODO(Guy or Marc): Return array of local images' URLs
              callback(_images);
              // For now returning List of images of type <Asset> using callback
              Navigator.pop(context); // Close view, go back
            },
          ),
          ListTile(
            enabled: (_images.length > 0 && _error == null),
            leading: Icon(Icons.cloud_upload),
            title: (_images.length > 0 && _error == null)
                ? Text("Upload images to server")
                : Text("No images selected"),
            onTap: () {
              _uploadImagesToServer();
              // TODO(Marc): Implement upload script
              // https://sh1d0w.github.io/multi_image_picker/#/upload?id=server-api
            },
          ),
          Expanded(
            child: buildGridView(),
          ),
        ],
      ),
    );
  }
}
