import "package:flutter/material.dart";
import 'package:multi_image_picker/multi_image_picker.dart';
import "package:provider/provider.dart";
import 'package:latlong/latlong.dart';

import '../models/block.dart';
import '../providers/blocks.dart';
import 'contribute_block_image_picker.dart';
import 'contribute_block_images_picker.dart';
import '../widgets/map_add_marker.dart';

class EditBlockScreen extends StatefulWidget {
  static const routeName = '/edit-block';

  @override
  _EditBlockScreenState createState() => _EditBlockScreenState();
}

class _EditBlockScreenState extends State<EditBlockScreen> {
  final _nameFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _locationFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editedBlock = Block(
    sector: 0,
    id: null,
    serverID: null,
    name: '',
    description: '',
    lat: null,
    lon: null,
    isDirty: true,
  );
  var _initValues = {
    'sector': 0,
    'name': '',
    'description': '',
    'lat': null,
    'lon': null,
  };
  var _isInit = true;
  var _isLoading = false;
  List _blockImages = <Asset>[];

  void _addPhotosFromImagePicker(List images) {
    setState(() {
      // _editedBlock.images = images;
      // TODO(Marc to Guy): How should we store these images coming from the image picker?
    });
    print("Images arrived in edit screen");
    setState(() {
      _blockImages = images;
    });
    print(_blockImages);
  }

  @override
  void dispose() {
    _nameFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _locationFocusNode.dispose();
    // _imageUrlFocusNode.removeListener(_updateImageUrl);
    // _imageUrlFocusNode.dispose();
    // _imageUrlController.dispose();
    super.dispose(); // AVOID MEMORY LEAKS
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      // Only execute when init and then check if input id can be found in db
      final blockId = ModalRoute.of(context).settings.arguments as String;

      if (blockId != null) {
        _editedBlock = Provider.of<BlockProvider>(context).getBlock(blockId);
        _initValues = {
          'name': _editedBlock.name,
          'sector': _editedBlock.sector,
          'description': _editedBlock.description,
          'lat': _editedBlock.lat,
          'lon': _editedBlock.lon,
          // 'imageUrl': '",
        };
        // _imageUrlController.text = _editedBlock.imageUrl;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  // Bottom sheet for displaying block information
  void _displaySetLocationMap(BuildContext context, LatLng latlng) {
    showModalBottomSheet(
        isDismissible: true,
        context: context,
        builder: (ctx) {
          return Container(
            height: MediaQuery.of(context).size.height * 0.6, // 60% height
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: AddMarkerToMap(
                    callback: setBlockPosition,
                    lat: _editedBlock.lat,
                    lon: _editedBlock.lon,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 10.0),
                  child: ElevatedButton.icon(
                    icon: const Icon(Icons.save),
                    label: const Text('Save'),
                    onPressed: () {
                      Navigator.pop(context);
                      print(
                          'Here we need to save the chosen LatLng to state...');
                    },
                  ),
                )
              ],
            ),
          );
        });
  }

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      // TODO: Throw snackbar dialog error message or something...
      return;
    }
    _form.currentState.save(); // Saves all inputs fields' states
    setState(() {
      _isLoading = true;
    });
    if (_editedBlock.id != null) {
      try {
        await Provider.of<BlockProvider>(
          context,
          listen: false,
        ).updateBlock(_editedBlock); // Updates block
      } catch (error) {
        await showDialog<Null>(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Updating block went wrong..."),
            content: Text(
              error.toString(),
            ),
            actions: <Widget>[
              TextButton(
                child: Text("Okay"),
                onPressed: () {
                  Navigator.of(ctx).pop(); // Close dialog
                },
              ),
            ],
          ),
        );
      }
    } else {
      try {
        await Provider.of<BlockProvider>(
          context,
          listen: false,
        ).addBlock(_editedBlock); // Adds block
      } catch (error) {
        await showDialog<Null>(
          context: context,
          builder: (ctx) => AlertDialog(
            title: Text("Adding block went wrong..."),
            content: Text(
              error.toString(),
            ),
            actions: <Widget>[
              TextButton(
                child: Text("Okay"),
                onPressed: () {
                  Navigator.of(ctx).pop(); // Close dialog
                },
              ),
            ],
          ),
        );
      }
    }
    setState(() {
      _isLoading = false;
    });
    Navigator.of(context).pop();
  }

  void setBlockPosition(LatLng latlng) {
    // TODO(From Marc to Guy): How should we save the information coming from
    // the map where the user chooses the block position to the _editedBlock state?
    // Shoudl we manage this newBlock globally somehow? Thanks!
    setState(() {
      _editedBlock.lat = latlng.latitude;
      _editedBlock.lon = latlng.longitude;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _editedBlock.id != null ? Text("Edit block") : Text("Add block"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _saveForm,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _saveForm,
        child: Icon(Icons.save),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _form,
                child: ListView(
                  children: <Widget>[
                    TextFormField(
                      initialValue: _initValues["name"],
                      decoration: InputDecoration(
                        labelText: "Block name",
                        hintText: "What's the name of this block?",
                        errorStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).errorColor,
                        ),
                      ),
                      textInputAction: TextInputAction.next,
                      focusNode: _nameFocusNode,
                      validator: (value) {
                        // Any string returned will be taken as error message here
                        if (value.isEmpty) {
                          return "Please enter a name.";
                        }
                        return null;
                      },
                      onFieldSubmitted: (_) {
                        FocusScope.of(context)
                            .requestFocus(_descriptionFocusNode);
                      },
                      onSaved: (value) {
                        _editedBlock =
                            _editedBlock.copyWith(name: value, isDirty: true);
                      },
                    ),
                    TextFormField(
                      initialValue: _initValues["description"],
                      decoration: InputDecoration(
                        labelText: "Description",
                        hintText: "Enter more details about the block.",
                      ),
                      // keyboardType: TextInputType.numberWithOptions(
                      //   decimal: true,
                      // ),
                      textInputAction: TextInputAction.newline,
                      onFieldSubmitted: (_) => {
                        _saveForm(),
                      },
                      focusNode: _descriptionFocusNode,
                      maxLines: 4,
                      keyboardType: TextInputType.multiline,
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Please enter a description";
                        }
                        if (value.length < 10) {
                          return "The description needs to be at least 10 characters long.";
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _editedBlock = _editedBlock.copyWith(
                            description: value, isDirty: true);
                      },
                    ),
                    SizedBox(
                      height: 25,
                    ), // Not the nicest solution think about alternatives
                    ListTile(
                      title: Text("Set block location on the map"),
                      leading: CircleAvatar(
                        child: Icon(Icons.location_searching),
                      ),
                      subtitle: Text(_editedBlock.lat != null &&
                              _editedBlock.lon != null
                          ? "Lat: ${_editedBlock.lat.toStringAsFixed(4)} Lon: ${_editedBlock.lon.toStringAsFixed(4)}"
                          : "No position set"),
                      onTap: () {
                        _displaySetLocationMap(
                          context,
                          LatLng(0, 0),
                        );
                      },
                    ),
                    ListTile(
                      title: Text("Add some photos of the block"),
                      leading: CircleAvatar(
                        child: Icon(Icons.image),
                      ),
                      subtitle: _blockImages.length > 0
                          ? Text("${_blockImages.length} images selected")
                          : Text("No photos attached"),
                      onTap: () {
                        Navigator.of(context).pushNamed(
                          ImagesPickerScreen.routeName,
                          arguments: _addPhotosFromImagePicker,
                        );
                      },
                    ),
                    ListTile(
                      title: Text("Pick and upload single photo"),
                      leading: CircleAvatar(
                        child: Icon(Icons.add_a_photo),
                      ),
                      subtitle: _blockImages.length > 0
                          ? Text("${_blockImages.length} images selected")
                          : Text("No photos attached"),
                      onTap: () {
                        Navigator.of(context).pushNamed(
                          ImagePickerScreen.routeName,
                          arguments: _addPhotosFromImagePicker,
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
