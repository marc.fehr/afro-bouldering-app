import 'dart:async';
import 'dart:ui' as ui;

import 'package:animated_floatactionbuttons/animated_floatactionbuttons.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:rxdart/subjects.dart';

class EditProblemDrawLineScreen extends StatefulWidget {
  static const routeName = '/edit-problem/draw-line';

  @override
  _EditProblemDrawLineScreenState createState() =>
      _EditProblemDrawLineScreenState();
}

class _EditProblemDrawLineScreenState extends State<EditProblemDrawLineScreen> {
  double opacity = 1.0;
  StrokeCap strokeType = StrokeCap.round;
  double strokeWidth = 5.0;
  Color selectedColor = Colors.red[400];
  double _imageWidth;
  double _imageHeight;
  var pointsList = <DrawModel>[];
  final pointsStream = BehaviorSubject<List<DrawModel>>();
  GlobalKey key = GlobalKey();
  final errorMessage = SnackBar(
    backgroundColor: Colors.red,
    content: Text('Oops, this line went out of the image...'),
  );
  final successMessage = SnackBar(
    backgroundColor: Colors.green,
    content: Text('Okay, line saved'),
  );

  translateAbsolutePointsToRelative() {
    var relativePointsList = <RelativePoint>[];

    RenderBox renderBox = key.currentContext.findRenderObject();
    double renderedImageWidth = renderBox.size.width;
    double renderedImageHeight = renderBox.size.height;

    for (int i = 0; i < pointsList.length; i++) {
      var relativeX = pointsList[i].offset.dx / renderedImageWidth;
      var relativeY = pointsList[i].offset.dy / renderedImageHeight;

      if (relativeX > 1.0 ||
          relativeY > 1.0 ||
          relativeX < 0 ||
          relativeY < 0) {
        pointsList.clear();
        print("Line was outside image!");
        ScaffoldMessenger.of(context).showSnackBar(errorMessage);
        return;
      }

      relativePointsList.add(
        RelativePoint(relativeX, relativeY),
      );

      print("–––$i–––");
      print("Relative X: $relativeX");
      print("Relative Y: $relativeY");
    }

    print(relativePointsList);
  }

  Widget colorMenuItem(Color color) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedColor = color;
        });
      },
      child: ClipOval(
        child: Container(
          padding: const EdgeInsets.only(bottom: 8.0),
          height: 36,
          width: 36,
          color: color,
        ),
      ),
    );
  }

  List<Widget> fabOptions() {
    return <Widget>[
      //FAB for resetting screen
      FloatingActionButton(
        heroTag: 'clear',
        child: Icon(Icons.delete),
        tooltip: 'Clear',
        onPressed: () {
          setState(
            () {
              pointsList.clear();
            },
          );
        },
      ),
      //FAB for resetting screen
      FloatingActionButton(
        heroTag: 'save',
        child: Icon(Icons.save),
        tooltip: 'Save',
        onPressed: () {
          setState(
            () {
              if (pointsList.length > 0) {
                ScaffoldMessenger.of(context).showSnackBar(successMessage);
              }
              // points.clear();
            },
          );
        },
      ),
      // FAB for picking red color
      FloatingActionButton(
        backgroundColor: Colors.white,
        heroTag: 'color_red',
        child: colorMenuItem(Colors.red),
        tooltip: 'Color',
        onPressed: () {
          setState(
            () {
              selectedColor = Colors.red;
            },
          );
        },
      ),
      // FAB for picking green color
      FloatingActionButton(
        backgroundColor: Colors.white,
        heroTag: 'color_green',
        child: colorMenuItem(Colors.green),
        tooltip: 'Color',
        onPressed: () {
          setState(
            () {
              selectedColor = Colors.green;
            },
          );
        },
      ),
      // FAB for picking pink color
      FloatingActionButton(
        backgroundColor: Colors.white,
        heroTag: 'color_pink',
        child: colorMenuItem(Colors.pink),
        tooltip: 'Color',
        onPressed: () {
          setState(
            () {
              selectedColor = Colors.pink;
            },
          );
        },
      ),
      // FAB for picking blue color
      FloatingActionButton(
        backgroundColor: Colors.white,
        heroTag: 'color_blue',
        child: colorMenuItem(Colors.blue),
        tooltip: 'Color',
        onPressed: () {
          setState(() {
            selectedColor = Colors.blue;
          });
        },
      ),
    ];
  }

  // Get image dimensions TODO(Marc): Add real image source...
  Future<Size> _calculateImageDimension() {
    Completer<Size> completer = Completer();
    Image image = Image.asset('assets/images/boulder_example_landscape.jpeg');
    image.image.resolve(ImageConfiguration()).addListener(
      ImageStreamListener(
        (ImageInfo image, bool synchronousCall) {
          var myImage = image.image;
          Size size = Size(myImage.width.toDouble(), myImage.height.toDouble());
          completer.complete(size);
        },
      ),
    );
    return completer.future;
  }

  @override
  void dispose() {
    pointsStream.close();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // Load image size and save to var
    _calculateImageDimension().then((size) {
      print("size = $size");
      setState(() {
        _imageWidth = size.width;
        _imageHeight = size.height;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Draw a line'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.save),
              onPressed: () {
                if (pointsList.length > 0) {
                  ScaffoldMessenger.of(context).showSnackBar(successMessage);
                }
                // print("Save line / points to problem provider");
              }),
          IconButton(
              icon: Icon(Icons.delete),
              color: Colors.red[300],
              onPressed: () {
                setState(
                  () {
                    pointsList.clear();
                  },
                );
              }),
        ],
      ),
      floatingActionButton: AnimatedFloatingActionButton(
        //Color shown when animation starts
        colorStartAnimation: Theme.of(context).primaryColor,
        //Color shown when animation ends
        colorEndAnimation: Theme.of(context).accentColor,
        //Icon for FAB. 'X' icon is shown when menu is open.
        animatedIconData: AnimatedIcons.menu_close,
        //Creating menu items
        fabButtons: fabOptions(),
      ),
      body: (_imageHeight != null &&
              _imageWidth != null) // Check image sizes available
          ? Column(
              children: <Widget>[
                Container(
                  key: key,
                  width: double.infinity,
                  height: (MediaQuery.of(context).size.width) /
                      _imageWidth *
                      _imageHeight,
                  child: GestureDetector(
                    onTapDown: (details) {
                      RenderBox renderBox =
                          key.currentContext.findRenderObject();
                      Paint paint = Paint();

                      paint.color = selectedColor;
                      paint.strokeWidth = strokeWidth;
                      paint.strokeCap = strokeType;

                      pointsList.add(
                        DrawModel(
                          offset:
                              renderBox.globalToLocal(details.globalPosition),
                          paint: paint,
                        ),
                      );

                      pointsStream.add(pointsList);
                    },
                    onPanStart: (details) {
                      RenderBox renderBox =
                          key.currentContext.findRenderObject();
                      Paint paint = Paint();

                      paint.color = selectedColor;
                      paint.strokeWidth = strokeWidth;
                      paint.strokeCap = strokeType;

                      pointsList.add(
                        DrawModel(
                          offset:
                              renderBox.globalToLocal(details.globalPosition),
                          paint: paint,
                        ),
                      );

                      pointsStream.add(pointsList);
                    },
                    onPanUpdate: (details) {
                      RenderBox renderBox =
                          key.currentContext.findRenderObject();
                      Paint paint = Paint();

                      paint.color = selectedColor;
                      paint.strokeWidth = strokeWidth;
                      paint.strokeCap = strokeType;

                      pointsList.add(
                        DrawModel(
                          offset:
                              renderBox.globalToLocal(details.globalPosition),
                          paint: paint,
                        ),
                      );

                      pointsStream.add(pointsList);
                    },
                    onPanEnd: (details) {
                      // pointsList.add(DrawModel(offset: null, paint: null));
                      // TODO: Save points to provider?
                      pointsStream.add(pointsList);
                      translateAbsolutePointsToRelative();
                    },
                    child: Stack(
                      children: <Widget>[
                        Image(
                          width: MediaQuery.of(context).size.width,
                          fit: BoxFit.contain,
                          image: AssetImage(
                              'assets/images/boulder_example_landscape.jpeg'),
                        ),
                        StreamBuilder<List<DrawModel>>(
                            stream: pointsStream.stream,
                            builder: (context, snapshot) {
                              return CustomPaint(
                                painter: DrawingPainter((snapshot.data ?? [])),
                                size: Size(
                                    double.infinity,
                                    (MediaQuery.of(context).size.width) /
                                        _imageWidth *
                                        _imageHeight),
                              );
                            }),
                      ],
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Theme.of(context).primaryColorLight,
                    width: double.infinity,
                    height: double.infinity,
                  ),
                )
              ],
            )
          : Center(
              child: CircularProgressIndicator(),
            ),
    );
  }
}

class DrawModel {
  final Offset offset;
  final Paint paint;

  DrawModel({
    this.offset,
    this.paint,
  });
}

class RelativePoint {
  final double x;
  final double y;

  RelativePoint(this.x, this.y);
}

class DrawingPainter extends CustomPainter {
  final List<DrawModel> pointsList;

  DrawingPainter(
    this.pointsList,
  );

  @override
  void paint(ui.Canvas canvas, ui.Size size) {
    for (int i = 0; i < pointsList.length - 1; i++) {
      // Check if there are at least two points
      if (pointsList[i] != null && pointsList[i + 1] != null) {
        canvas.drawLine(pointsList[i].offset, pointsList[i + 1].offset,
            pointsList[i].paint);
      } else if (pointsList[i] != null && pointsList[i + 1] == null) {
        List offsetList = <Offset>[];
        offsetList.add(pointsList[i]);
        canvas.drawPoints(ui.PointMode.points, offsetList, pointsList[i].paint);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
