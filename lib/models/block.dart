import 'package:meta/meta.dart';
import 'package:hive/hive.dart';
import 'dart:convert';

part 'block.g.dart';

// Block represents a physical boulder. It is contained in a unique sector,
// and may have a number of problems and/or photographs attached to it.
@HiveType(typeId: 0)
class Block {
  @HiveField(0)
  final String id; // local ID for reference in hive DB

  @HiveField(1)
  final int serverID; // server ID for usage in API

  @HiveField(2)
  final int sector; // sector ID the block belongs to

  @HiveField(3)
  double lat; // latitute of block position

  @HiveField(4)
  double lon; // longitude of block position

  @HiveField(5)
  final String name; // name of the block

  @HiveField(6)
  final String description; // description for the block

  @HiveField(7)
  final bool isDirty; // whether there are local changes to be synced

  Block({
    @required this.id,
    @required this.serverID,
    @required this.sector,
    @required this.lat,
    @required this.lon,
    @required this.name,
    @required this.description,
    @required this.isDirty,
  });

  Block copyWith({
    String id,
    int serverID,
    int sector,
    double lat,
    double lon,
    String name,
    String description,
    bool isDirty,
  }) =>
      Block(
        id: id ?? this.id,
        serverID: serverID ?? this.serverID,
        sector: sector ?? this.sector,
        lat: lat ?? this.lat,
        lon: lon ?? this.lon,
        name: name ?? this.name,
        description: description ?? this.description,
        isDirty: isDirty ?? this.isDirty,
      );

  factory Block.fromJson(String str) => Block.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Block.fromMap(Map<String, dynamic> json) => Block(
        serverID: json["id"], // we use server ID when hitting the API
        sector: json["sector"],
        lat: json["lat"] == null ? null : json["lat"].toDouble(),
        lon: json["lon"] == null ? null : json["lon"].toDouble(),
        name: json["name"],
        description: json["description"],

        // Blocks created from API responses are never dirty,
        // as they come directly from the server.
        isDirty: false,
      );

  Map<String, dynamic> toMap() => {
        "id": serverID, // we use the server ID when hitting the API
        "sector": sector,
        "lat": lat,
        "lon": lon,
        "name": name,
        "description": description,

        // For debugging:
        "localID": id,
      };
}
