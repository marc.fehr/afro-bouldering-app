import '../models/block.dart';
import '../providers/hive.dart';
import '../api/api.dart';

// BlockSyncer keeps block data in local storage in sync with block data on
// the server. Uploads are done with a "sweep", in which the syncer fires
// create/update API requests for every block that has been marked as dirty
// in the store. Sweeps can be triggered by changes on the Hive provider,
// but are also run periodically to catch transient errors.
class BlockSyncer {
  static const bool _debug = false; // enables debug logging

  HiveNotifier _hn;
  int _blockChanges;
  bool _blockMutex; // true if held - single threading makes this safe in Dart

  // BlockSyncer initialises and kicks off a new syncer instance. Multiple
  // syncers will likely race with each other and cause concurrency issues.
  BlockSyncer(HiveNotifier hn) {
    this._hn = hn;
    this._blockChanges = hn.blockChanges;
    this._blockMutex = false;

    // Listen for updates to the block store.
    this._hn.addListener(() {
      var newBlockChanges = this._hn.blockChanges;
      if (newBlockChanges > this._blockChanges) {
        this._blockChanges = newBlockChanges;
        this.uploadSweep(); // runs async in background
      }
    });
  }

  // uploadSweep creates or uploads a single dirty block to the server
  // and pushes the changes to the block store. This will trigger a
  // new sweep, so all dirty blocks are eventually uploaded.
  Future<void> uploadSweep() async {
    this._debugPrint('Syncer upload sweep has been triggered...');

    var bs = this._hn.getBlocks();
    this._debugPrint('#############');

    for (var b in bs) {
      this._debugPrint('${b.toJson()}');
      if (b.isDirty) {
        this._debugPrint('Syncer is uploading block ${b.id}.');
        this._debugPrint('#############\n');

        return this._uploadBlock(b);
      }
    }

    // No blocks to upload.
    this._debugPrint('Syncer has found nothing to do!');
    this._debugPrint('#############\n');
  }

  // _uploadBlock creates or uploads the given dirty block to the server. It
  // is guaranteed to cause a change notification on the underlying store.
  Future<void> _uploadBlock(Block b) async {
    // We can kill off accidental race conditions - the change guarantee means
    // that the sync loop will be triggered again after this anyway.
    if (this._blockMutex) {
      return; // another thread has the lock
    } else {
      this._blockMutex = true;
    }

    this._debugPrint('Uploading the following block:');
    this._debugPrint('${b.toJson()}\n');

    Block nb;
    if (b.serverID == null) {
      nb = await Api.createBlock(b);

      // We need to preserve the existing localID so we don't add a new copy
      // of b to the hive store. That would make maciej's servers happy =).
      nb = nb.copyWith(id: b.id, isDirty: false);
    } else {
      await Api.updateBlock(b);
      nb = b.copyWith(isDirty: false);
    }

    this._debugPrint('Pushing the following block to local storage:');
    this._debugPrint('${nb.toJson()}\n');

    await this._hn.putBlock(nb);
    this._blockMutex = false;
  }

  // _debugPrint prints a message when debug mode is enabled.
  void _debugPrint(String msg) {
    if (BlockSyncer._debug) {
      print(msg);
    }
  }
}
