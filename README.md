# bouldering_app

## Changes to the lib/models/block.dart package

We store models directly in a Hive database, which only supports primitive datatypes. To store more complex models, we generate type adapters automatically - if changes are made to models in this package, the following command might have to be run again:

```BASH
  flutter packages pub run build_runner build
```
