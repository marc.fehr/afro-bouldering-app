import 'package:test/test.dart';
import 'package:hive/hive.dart';

import '../../lib/models/block.dart';
import '../util.dart';

Future main() async {
  Hive.registerAdapter(BlockAdapter());
  await Hive.init(testDir());
  var box = await Hive.openBox('blocks_test');

  Block newTestBlock(String id) => new Block(
      id: id,
      serverID: 101,
      sector: 99,
      lat: 10.0,
      lon: 11.0,
      name: 'test',
      description: 'test_desc',
      isDirty: true);

  test('Hive should store and retrieve blocks correctly', () {
    var b1 = newTestBlock("1");
    box.put(b1.id, b1);
    var b2 = box.get(b1.id);

    expect(b1.id, equals(b2.id));
    expect(b1.serverID, equals(b2.serverID));
    expect(b1.sector, equals(b2.sector));
    expect(b1.lon, equals(b2.lon));
    expect(b1.lat, equals(b2.lat));
    expect(b1.name, equals(b2.name));
    expect(b1.description, equals(b2.description));
    expect(b1.isDirty, equals(b2.isDirty));
  });
}
