import 'package:test/test.dart';

import '../../lib/models/block.dart';

void main() {
  final testJson = """{
    "id": 1,
    "sector": 2,
    "lat": 10.0,
    "lon": 11.0,
    "name": "test",
    "description": "test_desc"
  }""";

  Block newTestBlock() => new Block(
      id: "1",
      serverID: 3,
      sector: 2,
      lat: 10.0,
      lon: 11.0,
      name: 'test',
      description: 'test_desc',
      isDirty: true);

  test('Block() should set fields correctly', () {
    var b = newTestBlock();

    expect(b.id, equals("1"));
    expect(b.serverID, equals(3));
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test'));
    expect(b.description, equals('test_desc'));
    expect(b.isDirty, equals(true));
  });

  test('copyWith() should override given fields', () {
    var b = newTestBlock().copyWith(id: "3", name: 'test2');

    expect(b.id, equals("3"));
    expect(b.serverID, equals(3));
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test2'));
    expect(b.description, equals('test_desc'));
    expect(b.isDirty, equals(true));
  });

  test('fromJson() should populate fields correctly', () {
    var b = Block.fromJson(testJson);

    expect(b.id, isNull);
    expect(b.serverID, equals(1)); // we use serverID for API stuff
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test'));
    expect(b.description, equals('test_desc'));
    expect(b.isDirty, equals(false)); // server responses aren't dirty
  });

  test('toJson() should output correct json', () {
    var b = Block.fromJson(newTestBlock().toJson());

    expect(b.id, isNull);
    expect(b.serverID, equals(3)); // we use serverID for API stuff
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test'));
    expect(b.description, equals('test_desc'));
    expect(b.isDirty, equals(false)); // server responses aren't dirty
  });
}
