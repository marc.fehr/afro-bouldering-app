import 'dart:async';

import 'package:test/test.dart';

import '../../lib/models/block.dart';
import '../../lib/providers/hive.dart';
import '../../lib/sync/block.dart';
import '../../lib/api/test_api.dart';
import '../../lib/api/api.dart';
import '../util.dart';

void main() async {
  // Point the API to a mock implementation.
  var ta = new TestApi();
  Api.swapImplementation(ta);

  // Initialise block syncer.
  HiveNotifier hn = await newHiveNotifierForTesting();
  BlockSyncer bs = new BlockSyncer(hn);

  Block newTestBlock(bool isDirty) => new Block(
      id: null, // will be populated by HiveNotifier
      serverID: null,
      sector: 2,
      lat: 10.0,
      lon: 11.0,
      name: 'test',
      description: 'test_desc',
      isDirty: isDirty);

  Future after(int msecs, Function cb) async {
    await new Future.delayed(Duration(milliseconds: msecs));
    return cb();
  }

  test('adding dirty block to store should trigger create', () async {
    var b = await hn.putBlock(newTestBlock(true));

    // TODO: add hooks to syncer to make this deterministic.
    await after(100, () {
      var nb = hn.getBlock(b.id);
      expect(nb.isDirty, isFalse); // no longer dirty
      expect(nb.serverID, isNotNull); // has a server ID
    });
  });

  test('dirtying block in store should trigger update', () async {
    var b = await Api.createBlock(newTestBlock(false));
    b = await hn.putBlock(b.copyWith(isDirty: true));

    // TODO: add hooks to syncer to make this deterministic.
    await after(100, () {
      var nb = hn.getBlock(b.id);
      expect(nb.isDirty, isFalse); // no longer dirty
      expect(nb.serverID, isNotNull); // has a server ID
    });
  });
}
