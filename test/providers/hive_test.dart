import 'package:test/test.dart';

import '../../lib/models/block.dart';
import '../../lib/providers/hive.dart';
import '../util.dart';

void main() async {
  HiveNotifier hn = await newHiveNotifierForTesting();

  test('adding a block should persist to hive db', () async {
    var bc = hn.blockChanges;
    var nb = await hn.putBlock(Block(serverID: 1, name: 'test'));

    // Block should have received an ID since it doesn't have one yet:
    expect(nb.id, isNotNull);
    var b = hn.getBlock(nb.id);

    // Block should have persisted correctly and updated change nonce.
    expect(hn.blockChanges, equals(bc + 1));
    expect(b.id, equals(nb.id));
    expect(b.serverID, equals(1));
    expect(b.name, equals('test'));

    // Clean-up.
    await hn.deleteBlock(b.id);
  });

  test('deleting a block should remove it from hive db', () async {
    var b = await hn.putBlock(Block(serverID: 1, name: 'test'));
    expect(hn.getBlock(b.id), isNotNull);

    var bc = hn.blockChanges;
    await hn.deleteBlock(b.id);
    expect(hn.blockChanges, equals(bc + 1));
    expect(hn.getBlock(b.id), isNull);
  });

  test('listing blocks should list all of them', () async {
    var b1 = await hn.putBlock(Block(serverID: 1, name: 'test1'));
    var b2 = await hn.putBlock(Block(serverID: 2, name: 'test2'));
    var b3 = await hn.putBlock(Block(serverID: 3, name: 'test3'));

    var bs = hn.getBlocks();
    expect(bs, hasLength(3));

    // Clean-up.
    await hn.deleteBlock(b1.id);
    await hn.deleteBlock(b2.id);
    await hn.deleteBlock(b3.id);
  });
}
