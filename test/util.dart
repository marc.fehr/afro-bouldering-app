import 'dart:math';

import 'package:test/test.dart';

import '../lib/providers/hive.dart';

// testDir generates a random temporary dir for testing Hive DBs.
String testDir() {
  var r = Random();
  return '/tmp/' +
      String.fromCharCodes(List.generate(5, (i) => r.nextInt(33) + 89));
}

// newHiveNotifierForTesting returns a clean HiveNotifier instance that
// writes to a hive DB stored in a random test directory.
Future<HiveNotifier> newHiveNotifierForTesting() async {
  await HiveNotifier.init(testDir());
  return HiveNotifier.newHiveNotifier();
}
