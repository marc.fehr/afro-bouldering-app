import 'package:test/test.dart';

import '../../lib/api/api.dart';
import '../../lib/api/rest_api.dart';
import '../../lib/models/block.dart';

void main() {
  // Ensure we test against the rest API.
  RestApi api = new RestApi();

  Block newTestBlock() => new Block(
        sector: 2,
        lat: 10.0,
        lon: 11.0,
        name: 'test',
        description: 'test_desc',
        isDirty: false,
      );

  test('getBlock() should correctly return block', () async {
    var b = await api.createBlock(newTestBlock());

    var nb = await api.getBlock(b.serverID);
    expect(nb.id, equals(b.id));
    expect(nb.serverID, equals(b.serverID));
    expect(nb.sector, equals(2));
    expect(nb.lat, equals(10.0));
    expect(nb.lon, equals(11.0));
    expect(nb.name, equals('test'));
    expect(nb.description, equals('test_desc'));

    await api.deleteBlock(b.serverID); // clean-up
  });

  test('listBlocks() should correctly return blocks', () async {
    var bs = await api.listBlocks(0);

    expect(bs.length, isNot(0));
    bs.forEach((b) => expect(b.id, isNot(0)));
  });

  test('createBlock() should correctly create block', () async {
    var b = await api.createBlock(newTestBlock());

    expect(b.id, isNull);
    expect(b.serverID, isNot(0));
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test'));
    expect(b.description, equals('test_desc'));

    await api.deleteBlock(b.serverID); // clean-up
  });

  test('updateBlock() should actually update blocks', () async {
    var b = await api.createBlock(newTestBlock());
    expect(b.serverID, isNot(0));

    var ub = b.copyWith(
        lat: 12.0, lon: 13.0, name: 'test2', description: 'test_desc2');
    await api.updateBlock(ub);

    var nb = await api.getBlock(b.serverID);
    expect(nb.id, isNull);
    expect(nb.serverID, isNot(0));
    expect(nb.sector, equals(b.sector));
    expect(nb.lat, equals(12.0));
    expect(nb.lon, equals(13.0));
    expect(nb.name, equals('test2'));
    expect(nb.description, equals('test_desc2'));

    await api.deleteBlock(b.serverID); // clean-up
  });

  test('deleteBlock() should actually delete blocks', () async {
    var b = await api.createBlock(newTestBlock());

    expect(b.id, isNull);
    expect(b.serverID, isNot(0));
    expect(b.sector, equals(2));
    expect(b.lat, equals(10.0));
    expect(b.lon, equals(11.0));
    expect(b.name, equals('test'));
    expect(b.description, equals('test_desc'));

    await api.deleteBlock(b.serverID);
    expect(() => api.getBlock(b.serverID), throwsException);
  });
}
